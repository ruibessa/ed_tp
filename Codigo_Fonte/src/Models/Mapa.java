package Models;

import Interfaces.IAposentos;
import Interfaces.IJogador;
import Interfaces.IMapa;
import classes.ArrayUnorderedList;
import classes.Graph;
import interfaces.UnorderedListADT;

import java.security.InvalidAlgorithmParameterException;

public class Mapa implements IMapa, Comparable<IJogador> {
    private String nome;
    private int pontos;
    private Graph<String> informacaoMapa;
    private UnorderedListADT<IAposentos> informacaoMapa2;
    //private OrderedListADT<IJogador> classificacaoJogadores = new ArrayOrderedList<>();

    public Mapa() {
        this.informacaoMapa = new Graph<String>();
        this.informacaoMapa2 = new ArrayUnorderedList<>();
    }

    public Mapa(String nome, int pontos, int sizeGraph) {
        this.nome = nome;
        this.pontos = pontos;
        this.informacaoMapa = new Graph<String>();
        this.informacaoMapa2 = new ArrayUnorderedList<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public UnorderedListADT<IAposentos> getInformacaoMapa2() {
        return informacaoMapa2;
    }

    public void setInformacaoMapa2(UnorderedListADT<IAposentos> informacaoMapa2) {
        this.informacaoMapa2 = informacaoMapa2;
    }

    @Override
    public Graph<String> getInformacaoMapa() {
        return informacaoMapa;
    }

    public void setInformacaoMapa(Graph<String> informacaoMapa) {
        this.informacaoMapa = informacaoMapa;
    }

    @Override
    public int compareTo(IJogador o) {
        return 0;
    }

    private String imprimeMapa() {
//        String resultado = "";
//        int contador = 0;
//        for (IAposentos fantasmas : informacaoMapa) {
//            resultado += fantasmas.toString();
//
//            if (contador != informacaoMapa.size() - 1) {
//                resultado += ", ";
//            }
//            contador++;
//        }
//        return resultado;
        return "TODO";
    }

    @Override
    public String toString() {
        return "{" +
                "Nome do mapa='" + nome + '\'' +
                ",Score Inicial=" + pontos + "\n" +
                ",Informacao=\n" + this.imprimeMapa() + "\n" +
                '}';
    }
}
