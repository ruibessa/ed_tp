package Models;

import Interfaces.*;
import classes.*;
import Enum.*;
import interfaces.*;

public class Fantasmas implements IFantasmas {
    private String aposento;
    private int fantasma;
    private UnorderedListADT<String> ligacoes = new ArrayUnorderedList<String>();

    public Fantasmas() {
    }

    public String getAposento() {
        return aposento;
    }

    public void setAposento(String aposento) {
        this.aposento = aposento;
    }

    public int getFantasma() {
        return fantasma;
    }

    public void setFantasma(int fantasma) {
        this.fantasma = fantasma;
    }

    public UnorderedListADT<String> getLigacoes() {
        return ligacoes;
    }

    public void setLigacoes(UnorderedListADT<String> ligacoes) {
        this.ligacoes = ligacoes;
    }

    private String imprimeLigacoes() {
        String resultado = "";
        int contador = 0;
        for (String conexao : ligacoes) {
            resultado += conexao.toString();

            if (contador != ligacoes.size() - 1) {
                resultado += ", ";
            }
            contador++;
        }
        return resultado;
    }

    public String toString(Grau grau) {
        if (grau.equals(Grau.BASICO)) {
            return "Fantasmas{" +
                    "aposento='" + aposento.toString() + '\'' +
                    ", fantasma=" + fantasma +
                    ", ligacoes=" + imprimeLigacoes().toString() +
                    '}' + "\n";
        } else if (grau.equals(Grau.NORMAL)) {
            return "Fantasmas{" +
                    "aposento='" + aposento.toString() + '\'' +
                    ", fantasma=" + fantasma +
                    ", ligacoes=" + imprimeLigacoes().toString()  +
                    '}' + "\n";
        }else {
            return "Fantasmas{" +
                    "aposento='" + aposento.toString() + '\'' +
                    ", ligacoes=" + imprimeLigacoes().toString() +
                    '}' + "\n";
        }
    }

    @Override
    public String toString() {
        return "Fantasmas{" +
                "aposento='" + aposento + '\'' +
                ", fantasma=" + fantasma +
                ", ligacoes=" + ligacoes +
                '}' + "\n";
    }
}
