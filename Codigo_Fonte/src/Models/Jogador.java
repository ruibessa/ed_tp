package Models;

import Interfaces.*;
import Enum.*;
import Public.SharedObj;

public class Jogador implements IJogador{
    private String nomeJogador;
    private int score;
    private Grau grau;
    private SharedObj obj;

    public Jogador() {
    }

    public Jogador(SharedObj iSharedObj) {
        this.obj = iSharedObj;
    }

    public Jogador(String nomeJogador, int score, Grau grau) {
        this.nomeJogador = nomeJogador;
        this.score = score;
        this.grau = grau;
    }

    public String getNomeJogador() {
        return nomeJogador;
    }

    public void setNomeJogador(String nomeJogador) {
        this.nomeJogador = nomeJogador;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Grau getGrau() {
        return grau;
    }

    public void setGrau(Grau grau) {
        this.grau = grau;
    }

    @Override
    public int compareTo(IJogador jogador) {
        return (this.getScore() < jogador.getScore()) ? -1 : (this.getScore() > jogador.getScore()) ? 1 : 0;
    }

    @Override
    public String toString() {
        return "{" +
                "Nome do jogador='" + nomeJogador + '\'' + "\n" +
                ",Score=" + score + "\n" +
                '}';
    }
}
