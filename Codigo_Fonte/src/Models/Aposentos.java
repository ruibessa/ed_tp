package Models;

import Interfaces.*;
import classes.*;
import Enum.*;
import interfaces.*;

public class Aposentos implements IAposentos {
    private String aposento;
    private int fantasma;
    private UnorderedListADT<String> ligacoes;

    public Aposentos() {
        ligacoes = new ArrayUnorderedList<String>();
    }

    public Aposentos(String aposento, int fantasma) {
        this.aposento = aposento;
        this.fantasma = fantasma;
        ligacoes = new ArrayUnorderedList<String>();
    }

    public String getAposento() {
        return aposento;
    }

    public void setAposento(String aposento) {
        this.aposento = aposento;
    }

    public int getFantasma() {
        return fantasma;
    }

    public void setFantasma(int fantasma) {
        this.fantasma = fantasma;
    }

    public UnorderedListADT<String> getLigacoes() {
        return ligacoes;
    }

    public void setLigacoes(UnorderedListADT<String> ligacoes) {
        this.ligacoes = ligacoes;
    }

    private String imprimeLigacoes() {
        String resultado = "";
        int contador = 0;
        for (String conexao : ligacoes) {
            resultado += conexao.toString();

            if (contador != ligacoes.size() - 1) {
                resultado += ", ";
            }
            contador++;
        }
        return resultado;
    }

    public String toString(Grau grau) {
        if (grau.equals(Grau.BASICO)) {
            return "Aposentos{" +
                    "aposento='" + aposento.toString() + '\'' +
                    ", fantasma=" + fantasma +
                    ", ligacoes=" + imprimeLigacoes().toString() +
                    '}' + "\n";
        } else if (grau.equals(Grau.NORMAL)) {
            return "Aposentos{" +
                    "aposento='" + aposento.toString() + '\'' +
                    ", fantasma=" + fantasma +
                    ", ligacoes=" + imprimeLigacoes().toString() +
                    '}' + "\n";
        } else {
            return "Aposentos{" +
                    "aposento='" + aposento.toString() + '\'' +
                    ", ligacoes=" + imprimeLigacoes().toString() +
                    '}' + "\n";
        }
    }

    @Override
    public String toString() {
        return "Aposentos{" +
                "aposento='" + aposento + '\'' +
                ", fantasma=" + fantasma +
                ", ligacoes=" + ligacoes +
                '}' + "\n";
    }
}
