package Demo;

import java.util.*;
import Configs.*;
import Ficheiro.*;
import Interfaces.*;
import Models.*;
import Enum.*;
import Public.SharedObj;

public class Demo {
    public static void main(String[] args) {
        SharedObj obj = new SharedObj();
        Paths paths = new Paths();
        ParseJson parseJson = new ParseJson(obj);
        IJogador jogador = new Jogador();

        boolean verificar=true;
        int opcao_menu = 0;
        do {
            System.out.println("\nMenu - Casa Assombrada\n");
            System.out.println("1 - Carregar mapa");
            System.out.println("2 - Visualizar mapa");
            System.out.println("3 - Visualizar rank");
            System.out.println("4 - Jogar modo manual");
            System.out.println("5 - Jogar modo simulacao");
            System.out.println("6 - Guardar ficheiro");
            System.out.println("7 - Criar perfil do jogador");
            System.out.println("0 - Sair");

            Scanner sc = new Scanner(System.in);
            System.out.println("Opcao:");
            opcao_menu = sc.nextInt();
            System.out.print("\n");
            switch (opcao_menu) {

                case 0:
                    System.out.println("Bye Bye");
                    System.exit(0);
                    break;

                case 1:
                    boolean verificarCarregamento = false;
                    verificarCarregamento = parseJson.readJson(paths.getPathMapaRead());
                    if(verificarCarregamento = true) {
                        System.out.println("Mapa carregado com sucesso.");
                    } else {
                        System.out.println("Erro ao carregar o mapa.");
                    }
                    break;

                case 2:
                    Scanner teclado = new Scanner(System.in);
                    int temp_Dificuldade = 0;
                    System.out.println("Grau de dificuldade");
                    System.out.println("1 - Basico");
                    System.out.println("2 - Normal");
                    System.out.println("3 - Dificil");

                    do {
                        System.out.println("Grau de dificuldade:\t");
                        temp_Dificuldade = teclado.nextInt();

                    } while (temp_Dificuldade<1 || temp_Dificuldade>3);

                    if(temp_Dificuldade == 1) {
                        jogador.setGrau(Grau.BASICO);
                    } else if (temp_Dificuldade == 2) {
                        jogador.setGrau(Grau.NORMAL);
                    }
                    else if (temp_Dificuldade == 3) {
                        jogador.setGrau(Grau.DIFICIL);
                    }

                    System.out.println("Informações do mapa\n");
                    System.out.println(parseJson.toString());
                    break;

                case 3:
                    System.out.println("3 - Visualizar rank");
                    break;

                case 4:
                    System.out.println("4 - Jogar modo manual");
                    break;

                case 5:
                    System.out.println("5 - Jogar modo simulacao");
                    break;

                case 6:
                    System.out.println("6 - Guardar ficheiro");
                    break;

                case 7:
                    if(verificar) {
                        Scanner tecladoCriarPerfil = new Scanner(System.in);
                        String tempNomeJogador;

                        System.out.println("Nome do jogador:\t");
                        tempNomeJogador = tecladoCriarPerfil.nextLine();
                        IJogador tempJogador = new Jogador(tempNomeJogador, 0 , Grau.BASICO);
                        verificar=false;
                    } else {
                        System.out.println("Ja existe um perfil criado");
                    }
                    break;
                default:
                    System.out.println("Opcao Invalida!");
                    break;
            }
        } while (opcao_menu != 0);
    }
}