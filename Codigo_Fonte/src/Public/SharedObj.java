package Public;

import Interfaces.*;
import interfaces.*;
import classes.*;

public class SharedObj<T>{

    private UnorderedListADT<T> aposentosLigados;
    private int quantidadeAposentos;

    public SharedObj() {
        this.aposentosLigados = new ArrayUnorderedList<>();
        this.quantidadeAposentos = 0;
    }

    public int getQuantidadeAposentos() {
        return quantidadeAposentos;
    }

    public void setQuantidadeAposentos(int quantidadeAposentos) {
        this.quantidadeAposentos = quantidadeAposentos;
    }

    public UnorderedListADT<T> getAposentosLigados() {
        return aposentosLigados;
    }
    public void setAposentosLigados(UnorderedListADT<T> aposentosLigados) {
        this.aposentosLigados = aposentosLigados;
    }

    @Override
    public String toString() {
        return "SharedObj{" +
                "aposentosLigados=" + aposentosLigados +
                '}';
    }
}
