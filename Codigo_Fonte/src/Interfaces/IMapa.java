package Interfaces;

import classes.Graph;
import interfaces.UnorderedListADT;

public interface IMapa extends Comparable<IJogador>{

    public String getNome();

    public void setNome(String nome);

    public int getPontos();

    public void setPontos(int pontos);

    public Graph<String> getInformacaoMapa();

    public void setInformacaoMapa(Graph<String> informacaoMapa);

    public UnorderedListADT<IAposentos> getInformacaoMapa2();

    @Override
    int compareTo(IJogador o);
}

