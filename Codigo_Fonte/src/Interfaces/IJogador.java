package Interfaces;

import Enum.*;

public interface IJogador extends Comparable<IJogador> {

    public String getNomeJogador();

    public void setNomeJogador(String nomeJogador);

    public int getScore();

    public void setScore(int score);

    public Grau getGrau();

    public void setGrau(Grau grau);

    @Override
    int compareTo(IJogador jogador);

}
