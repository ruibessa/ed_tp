package Interfaces;

import interfaces.*;
import Enum.*;

public interface IAposentos {

    public String getAposento();

    public void setAposento(String aposento);

    public int getFantasma();

    public void setFantasma(int fantasma);

    public UnorderedListADT<String> getLigacoes();

    public void setLigacoes(UnorderedListADT<String> ligacoes);
}
