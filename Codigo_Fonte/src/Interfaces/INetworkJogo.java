package Interfaces;

import interfaces.NetworkADT;

import java.util.Iterator;

public interface INetworkJogo<T> extends NetworkADT<T> {
    public String isCompleto();

    public Iterator<T> caminhoMaisCurto(int aposento1, int aposento2);

    public Iterator<T> listarLigacoes(int aposento);

    public Iterator<T> listarLigacoesViaUmIntermediario(int aposento);

    public Iterator<T> listarNaoLigados(int aposento);

    public void imprimirMatriz();
}
