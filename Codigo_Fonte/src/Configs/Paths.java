package Configs;

public class Paths {

    private String pathMapaRead = "bd/mapa.json";
    private String pathMapaSave = "save/informacao.json";

    public String getPathMapaRead() {
        return pathMapaRead;
    }

    public void setPathMapaRead(String pathMapaRead) {
        this.pathMapaRead = pathMapaRead;
    }

    public String getPathMapaSave() {
        return pathMapaSave;
    }

    public void setPathMapaSave(String pathMapaSave) {
        this.pathMapaSave = pathMapaSave;
    }

    @Override
    public String toString() {
        return "Paths{" +
                "pathMapaRead='" + pathMapaRead + '\'' +
                ", pathMapaSave='" + pathMapaSave + '\'' +
                '}';
    }
}
