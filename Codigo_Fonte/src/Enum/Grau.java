package Enum;

public enum Grau {
    BASICO, NORMAL, DIFICIL;

    @Override
    public String toString() {
        switch (this) {
            case BASICO:
                return "Basico";
            case NORMAL:
                return "Normal";
            case DIFICIL:
                return "Dificil";
            default:
                return "Nenhum";
        }
    }
}
