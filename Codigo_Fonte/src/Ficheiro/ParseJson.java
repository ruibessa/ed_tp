package Ficheiro;

import Interfaces.IAposentos;
import Interfaces.IMapa;
import Public.SharedObj;

import org.json.simple.*;
import org.json.simple.parser.*;
import Models.*;
import classes.*;
import interfaces.*;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class ParseJson {
    private JSONParser parser;
    private UnorderedListADT<String> arrayLigacoes;
    private IAposentos aposentos;
    private IMapa mapa;
    private SharedObj obj;

    public ParseJson(SharedObj sharedObj) {
        this.parser = new JSONParser();
        this.arrayLigacoes = new ArrayUnorderedList<String>();
        this.mapa = new Mapa();
        this.obj = sharedObj;
    }

    public ParseJson() {
    }

    /*
     * Metodo para leitora do ficheiro
     */
    public boolean readJson(String path) {
        try {
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(path));

            JSONArray jsonMapa = (JSONArray) jsonObject.get("mapa");
            this.mapa = new Mapa((String) jsonObject.get("nome"), (int) (long) jsonObject.get("pontos"), jsonMapa.size() + 2);


            IAposentos aposentoEntrada = new Aposentos("entrada", 0);
            mapa.getInformacaoMapa2().addToRear(aposentoEntrada);
            mapa.getInformacaoMapa().addVertex("entrada");

            int contAposento = 1;

            for (Object objectMapa : jsonMapa) {
                aposentos = new Aposentos();
                JSONObject jsonObjectMapa = (JSONObject) objectMapa;
                aposentos.setAposento((String) jsonObjectMapa.get("aposento"));
                aposentos.setFantasma((int) (long) jsonObjectMapa.get("fantasma"));
                JSONArray tempLigacoes = (JSONArray) jsonObjectMapa.get("ligacoes");
                arrayLigacoes = new ArrayUnorderedList<String>();

                for (Object conexao : tempLigacoes) {
                    String string = (String) conexao;
                    arrayLigacoes.addToRear(string);
                }

                aposentos.setLigacoes(arrayLigacoes);

                mapa.getInformacaoMapa2().addToRear(aposentos);
                mapa.getInformacaoMapa().addVertex((String) jsonObjectMapa.get("aposento"));

                contAposento++;
            }


            IAposentos aposentoExterior = new Aposentos("exterior", 0);
            mapa.getInformacaoMapa2().addToRear(aposentoExterior);
            mapa.getInformacaoMapa().addVertex("exterior");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        System.out.println(mapa.getInformacaoMapa().getIndex("cozinha"));

        //atualizar as ligacoes da matriz para true nas arestas
        Iterator<IAposentos> it = mapa.getInformacaoMapa2().iterator();
        int contadorIndexOrigem = -1;

        while (it.hasNext()) { //vertice a vertice
            contadorIndexOrigem++;
            //System.out.println(it.next().getAposento());

            for (String conexao : it.next().getLigacoes()) { //ligacao a ligacao
                int indiceDestino = mapa.getInformacaoMapa().getIndex(conexao);
                mapa.getInformacaoMapa().addEdge(contadorIndexOrigem, indiceDestino);

            }

        }

        //mapa.getInformacaoMapa().addEdge(contadorIndexDestino, contAposento, 1);

        System.out.println("\n\n");
        mapa.getInformacaoMapa().imprimirMatriz();

        return true;
    }

    @Override
    public String toString() {
        return "{" + mapa.toString() +
                obj +
                '}';
    }
}
