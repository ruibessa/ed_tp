package mapa;

import Interfaces.INetworkJogo;
import classes.Network;

import java.util.Iterator;

public class NetworkJogo<T> extends Network<T> implements INetworkJogo<T> {
    protected int[][] adjMatrix;

    public int[][] getAdjMatrix() {
        return adjMatrix;
    }

    public void setAdjMatrix(int[][] adjMatrix) {
        this.adjMatrix = adjMatrix;
    }

    public NetworkJogo(int size) {
        numVertices = 0;
        this.adjMatrix = new int[size][size];
        this.vertices = (T[]) (new Object[size]);
        for (int i = 0; i < size; i++) {
            for (int j = size - 1; j >= 0; j--) {
                adjMatrix[i][j] = 0;
            }
        }
    }

    @Override
    public String isCompleto() {
        return null;
    }

    @Override
    public Iterator<T> caminhoMaisCurto(int aposento1, int aposento2) {
        return null;
    }

    @Override
    public Iterator<T> listarLigacoes(int aposento) {
        return null;
    }

    @Override
    public Iterator<T> listarLigacoesViaUmIntermediario(int aposento) {
        return null;
    }

    @Override
    public Iterator<T> listarNaoLigados(int aposento) {
        return null;
    }

    public void imprimirMatriz() {
        for (int i = 0; i < adjMatrix.length; i++) {
            System.out.print("| ");
            for (int j = 0; j < adjMatrix.length; j++) {
                System.out.print(adjMatrix[i][j] +" | ");
            }
            System.out.println("\n");
        }
    }
}
